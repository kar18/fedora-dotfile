#!/bin/bash

# Display a GUI prompt asking if the user wants to download and install osu!lazer
zenity --question --text="Do you want to download and install osu!lazer?"

# Check the exit status of the zenity command
if [ $? = 0 ]; then
    # Fetch the latest version number from GitHub releases
    latest_version=$(curl -s https://api.github.com/repos/ppy/osu/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")')

    # Construct the download URL for the latest version of osu!lazer AppImage
    download_url="https://github.com/ppy/osu/releases/download/$latest_version/osu.AppImage"

    # Define the file name for the downloaded AppImage
    appimage_file="osu-lazer-$latest_version.AppImage"

    # Download the latest version
    curl -L -o "$appimage_file" "$download_url"

    # Give execution permissions to the AppImage
    chmod +x "$appimage_file"

    # Inform the user
    zenity --info --text="osu!lazer AppImage ($latest_version) has been downloaded and is ready to run."

    # Run the AppImage
    ./"$appimage_file"
else
    # Inform the user about the cancellation
    zenity --info --text="Download and installation of osu!lazer canceled."
fi
