
install_packages() {
    if [ -f /etc/lsb-release ]; then
        sudo apt-get update
        sudo apt-get install -y scrot feh
    elif [ -f /etc/arch-release ]; then
        sudo pacman -Sy scrot feh
    elif [ -f /etc/redhat-release ]; then
        sudo dnf install -y scrot feh
    else
        echo "Unsupported distribution. Please install scrot and feh manually. If you are certain that these packages are already installed, you can safely ignore this message." && mkdir -p ~/.tmp_screenshot && cd  ~/.tmp_screenshot && scrot -s ~/.tmp_screenshot/temp.png && mv ~/.tmp_screenshot/temp.png ~/.tmp_screenshot && cd -

    fi
}

if ! command -v scrot &> /dev/null || ! command -v feh &> /dev/null; then
    echo "scrot and/or feh are not installed."

    install_packages

    if [ $? -eq 0 ]; then
        echo "scrot and feh have been installed."
    else
        echo "Failed to install scrot and feh."
    fi
else
    echo "scrot and feh are already installed." && mkdir -p ~/.tmp_screenshot && cd  ~/.tmp_screenshot && scrot -s ~/.tmp_screenshot/temp.png && mv ~/.tmp_screenshot/temp.png ~/.tmp_screenshot/temp_000.png && feh --borderless --title 'Image Viewer' temp_000.png && cd -
fi
