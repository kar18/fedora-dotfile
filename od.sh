# Specify the download URL
download_url="https://github.com/OpenTabletDriver/OpenTabletDriver/releases/latest/download/OpenTabletDriver.rpm"

# Specify the destination file path
destination_file="OpenTabletDriver.rpm"

# Download the RPM
curl -L -o "$destination_file" "$download_url"

# Install the RPM
sudo dnf install -y "$destination_file"

# Clean up: remove the downloaded RPM
rm "$destination_file"


