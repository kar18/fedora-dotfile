#!/bin/bash

# Exit on error
set -e

# Define configurations
CONFIGURATIONS=(
  'fastestmirror=True'
  'max_parallel_downloads=3'
  'keepcache=True'
  'color=always'
)

# Path to the dnf.conf file
DNF_CONF_PATH="/etc/dnf/dnf.conf"

add_configuration() {
  for config in "${CONFIGURATIONS[@]}"; do
    if ! grep -q "^$config" "$DNF_CONF_PATH"; then
      echo "$config" | sudo tee -a "$DNF_CONF_PATH"
    else
      echo "Configuration '$config' already exists in $DNF_CONF_PATH"
    fi
  done
}

add_configuration

# this will do update and upgrade
sudo dnf upgrade --refresh -y
sudo dnf install zsh util-linux-user -y  #(powerful shell bash alternative )
chsh -s $(which zsh)  #make ZSH the default shell
sudo chmod +x ssdm_i3.sh
#To enable the Free repository, use:
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
#sudo dnf groupupdate core && # if you are using the Gnome desktop environment.
sudo dnf copr enable atim/starship -y
#sudo dnf install fedy -y

#Optionally, enable the Nonfree repository:
#sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
curl -fsSL https://rpm.librewolf.net/librewolf-repo.repo | pkexec tee /etc/yum.repos.d/librewolf.repo
curl -f https://zed.dev/install.sh | sh


packages=(
    # From copr
    "starship"
    # from official dnf
    "flatpak"
    "lxpolkit"
    "snapd"
    "sqlite-devel"                  # SQLite development files
    "gimagereader-qt"               # GImageReader with Qt interface
    "i3"                            # i3 window manager
    "dnfdragora"                    # DNF Dragora - DNF frontend with GUI appstore
    "picom"                         # Compositor for Xorg with opacity/transparency
    "fedora-packager fedora-review" # Tools for Fedora package maintainers
    "feh"                           # Fast and light image viewer and wallpaper manager
    "copyq"                         # Clipboard manager with advanced features
    "dunst"                         # Lightweight and customizable notification daemon
    "timeshift"                     # System restore tool for Linux
    "minetest"                      # Open-source Minecraft alternative
    "sxhkd"                         # Simple X hotkey daemon
    "rofi"                          # Application launcher, window switcher, and dmenu replacement
    "konsole"                       # Terminal for use inside Dolphin
    "dolphin"                       # KDE file manager
    "udiskie"                       # Automounter for removable media
    "volumeicon"                    # Lightweight volume control
    "flameshot"                     # Powerful yet simple screenshot software
    "kde-partitionmanager"          # Partition editor for creating, reorganizing, and deleting partitions
    "mpv"                           # Command line video player
    "qbittorrent"                   # qBittorrent BitTorrent client
    "bleachbit"                     # BleachBit disk space cleaner, privacy manager, and system optimizer
    "kdeconnectd"                   # Multi-platform app that allows devices to communicate
  #  "fedy"                  # Enable Fedy COPR repository for third-party software
   # "fedy"                          # Fedy makes it easy to install third-party software in Fedora
    "slock"                         # Simple X display locker
    "librewolf"        		# librewolf Browser
    "librewolf"                     # Privacy-focused Firefox browser fork
    "fastfetch"                     # Fast system information script
    "tilda"                         # Drop-down terminal
    "slock"                         # Simple X display locker
    "blueman"                       # Bluetooth manager
    "xkill"                         # Utility for forcefully terminating X clients
    "vim"                           # Improved version of the vi text editor
    "lxappearance"                  # GTK theme switcher
    "fontawesome-fonts-all"         # Font Awesome fonts
    "./rpm/Ferdium-linux-6.7.6-x86_64.rpm" # Install specific RPM package
    "./rpm/appimagelauncher-2.2.0-travis995.0f91801.x86_64.rpm" # Install specific RPM package
    "nitrogen"                      # Background browser and setter for X windows
    "meld"                          # Visual diff and merge tool
    "neovim"                        # Vim-fork focused on extensibility and usability
    "kwrite"                        # Text editor for KDE
    "automake ibus-devel"           # Install packages for Bangla keyboard in IBus
)

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo   #! adding flatpak

# Loop through the array and install each package
for package in "${packages[@]}"; do
    sudo dnf install $package -y
done



git clone https://github.com/sarim/ibus-avro.git || echo "exists ============" &&
cd ibus-avro &&
aclocal && autoconf && automake --add-missing &&
./configure --prefix=/usr &&
cd - &&


#Install Preload: if you have upto 16gb ram enable it
sudo dnf copr enable elxreno/preload -y && sudo dnf install preload -y
#copy all the configuration file(nonehide dir) &&

if [ -n "$(find ./dotfile/ -mindepth 1 -print -quit)" ]; then
    cp -rvi ./dotfile/* ~/
else
    echo "Source directory does not exist or is empty."
fi


#if [ -n "$(find ./dotfile/ -mindepth 1 -maxdepth 1 -print -quit)" ]; then
#    cp -rvi ./dotfile/.* ~/
#else
#    echo "Source directory does not exist or is empty."
#fi

sudo cp -rf ./usr/share/fonts/* /usr/share/fonts/
sudo cp -rf ./usr/share/icons/* /usr/share/icons/
sudo cp -rf ./usr/share/themes/* /usr/share/themes/

sudo chmod +x ./rpm/theme/Orchis-theme-git/install.sh
./rpm/theme/Orchis-theme-git/install.sh -t purple --tweaks compact

sudo firewall-cmd --permanent --zone=public --add-service=kdeconnect
sudo firewall-cmd --reload

install_open_tablet_driver() {
  # Specify the download URL
  local download_url="https://github.com/OpenTabletDriver/OpenTabletDriver/releases/latest/download/OpenTabletDriver.rpm"

  # Specify the destination file path
  local destination_file="OpenTabletDriver.rpm"

  # Download the OpenTabletDriver RPM
  curl -L -o "$destination_file" "$download_url"

  # Install the OpenTabletDriver RPM
  sudo dnf install -y "$destination_file"

  # Clean up: remove the downloaded OpenTabletDriver RPM
  rm "$destination_file"
}
install_open_tablet_driver
systemctl --user enable opentabletdriver.service --now

install_osu_lazer() {
    # Fetch the latest version number from GitHub releases
    latest_version=$(curl -s https://api.github.com/repos/ppy/osu/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")')

    # Construct the download URL for the latest version of osu!lazer AppImage
    download_url="https://github.com/ppy/osu/releases/download/$latest_version/osu.AppImage"

    # Define the file name for the downloaded AppImage
    appimage_file="osu-lazer-$latest_version.AppImage"

    # Download the latest version
    curl -L -o "$appimage_file" "$download_url"

    # Give execution permissions to the AppImage
    chmod +x "$appimage_file"

    # Run the AppImage
    ./"$appimage_file"
    rm "$appimage_file"
}

install_osu_lazer

#guake --restore-preferences ~/.config/guake/guake_conf && # restore guake terminal

#Install Media Codecs fix sound problem
# sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
# sudo dnf groupupdate sound-and-video

#cd ~/Pictures &&
#git clone --depth 1 https://gitlab.com/krafi/personal-wallpaper-collection.git | echo "exits====" && cd - &&
#cd ~/Pictures/personal-wallpaper-collection &&
#./set_nitrogen.sh &&

sudo chmod +x appimage.sh
./appimage.sh

echo "wana set-up 'systemctl graphical.target' run ./ssdm_i3.sh dont know what is that ? just skip"
echo "if you want partation manager type 'sudo dnf install gparted fdisk'"
echo "if config file didn't work properly 'cp -rf ./dotfile/.* ~/'"
echo "Congratulation, Installation has been successful. Please restart your system"

#ls ~/Pictures/personal-wallpaper-collection/Wallpapers@krafi.info
